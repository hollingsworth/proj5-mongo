'''
Test cases for ACP calculator
'''
import nose
from acp_times import * 

start_time = arrow.get(2020, 1, 1) #2020-1-1 00:00:00

def test_example_1():
    #Test controls at 60, 120, 175, 205 for a 200km brevet
    dist = 200
    #control at 60
    assert open_time(60, dist, start_time.isoformat()) == start_time.shift(hours=1, minutes=46).isoformat()
    assert close_time(60, dist, start_time.isoformat()) == start_time.shift(hours=4, minutes=0).isoformat()
    #control at 120
    assert open_time(120, dist, start_time.isoformat()) == start_time.shift(hours=3, minutes=32).isoformat()
    assert close_time(120, dist, start_time.isoformat()) == start_time.shift(hours=8, minutes=0).isoformat()
    #control at 175
    assert open_time(175, dist, start_time.isoformat()) == start_time.shift(hours=5, minutes=9).isoformat()
    assert close_time(175, dist, start_time.isoformat()) == start_time.shift(hours=11, minutes=40).isoformat()
    #control at 205, past finish
    assert open_time(205, dist, start_time.isoformat()) == start_time.shift(hours=5, minutes=53).isoformat()
    assert close_time(205, dist, start_time.isoformat()) == start_time.shift(hours=13, minutes=30).isoformat()

def test_example_2():
    #Test controls at 100, 200, 550, 600 for a 600km brevet
    dist = 600
    #control at 100
    assert open_time(100, dist, start_time.isoformat()) == start_time.shift(hours=2, minutes=56).isoformat()
    #control at 200
    assert open_time(200, dist, start_time.isoformat()) == start_time.shift(hours=5, minutes=53).isoformat()
    #control at 550
    assert open_time(550, dist, start_time.isoformat()) == start_time.shift(hours=17, minutes=8).isoformat()
    assert close_time(550, dist, start_time.isoformat()) == start_time.shift(hours=36, minutes=40).isoformat()
    #control at 600
    assert close_time(600, dist, start_time.isoformat()) == start_time.shift(hours=40, minutes=00).isoformat()

def test_example_3():
    #Test control at 890 for a 1000km brevet
    dist = 1000
    #control at 890
    assert open_time(890, dist, start_time.isoformat()) == start_time.shift(hours=29, minutes=9).isoformat()
    assert close_time(890, dist, start_time.isoformat()) == start_time.shift(hours=65, minutes=23).isoformat()

def test_finish_distances():
    #Test the finish point for each possible brevet distance
    dist = 200
    assert open_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=5, minutes=53).isoformat()
    assert close_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=13, minutes=30).isoformat()
    dist = 300
    assert open_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=9, minutes=0).isoformat()
    assert close_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=20, minutes=0).isoformat()
    dist = 400
    assert open_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=12, minutes=8).isoformat()
    assert close_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=27, minutes=0).isoformat()
    dist = 600
    assert open_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=18, minutes=48).isoformat()
    assert close_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=40, minutes=0).isoformat()
    dist = 1000
    assert open_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=33, minutes=5).isoformat()
    assert close_time(dist, dist, start_time.isoformat()) == start_time.shift(hours=75, minutes=0).isoformat()

def test_over_finish_distances():
    #Test finish slightly over the brevet distance has the same times as finish at the exact brevet distance
    dist = 200
    assert open_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=5, minutes=53).isoformat()
    assert close_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=13, minutes=30).isoformat()
    dist = 300
    assert open_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=9, minutes=0).isoformat()
    assert close_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=20, minutes=0).isoformat()
    dist = 400
    assert open_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=12, minutes=8).isoformat()
    assert close_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=27, minutes=0).isoformat()
    dist = 600
    assert open_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=18, minutes=48).isoformat()
    assert close_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=40, minutes=0).isoformat()
    dist = 1000
    assert open_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=33, minutes=5).isoformat()
    assert close_time(dist + 20, dist, start_time.isoformat()) == start_time.shift(hours=75, minutes=0).isoformat()

def test_oddities_french_rules():
    #Test controls <=60 use the French rules
    dist = 200
    assert close_time(20, dist, start_time.isoformat()) == start_time.shift(hours=2, minutes=0).isoformat()
    assert close_time(60, dist, start_time.isoformat()) == start_time.shift(hours=4, minutes=0).isoformat()

def test_oddities_start_point_control():
    #Test control at the starting point closes at 1H0
    dist = 200
    assert close_time(0, dist, start_time.isoformat()) == start_time.shift(hours=1, minutes=0).isoformat()
    
