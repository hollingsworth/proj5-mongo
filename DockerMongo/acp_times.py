"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import logging

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def convert_time(time):
   hours = int(time//1) 
   #minutes = (time * 60) % 60
   minutes = round(((time*60) %60))
   return [hours, minutes]

#start location, end location, minmum speed, maximum speed
speeds = [[0, 200, 15, 34], [200, 400, 15, 32], [400, 600, 15, 30], [600, 1000, 11.428, 28], [1000, 1300, 13.333, 26]]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
   """
   time = arrow.get(brevet_start_time)
   for li in speeds:
      start_loc, end_loc, min_speed, max_speed = li
      if control_dist_km >= brevet_dist_km:
      #rules for finish points, cleanup later
         if brevet_dist_km == 200:
            return time.shift(hours = 5, minutes = 53).isoformat()
         if brevet_dist_km == 300:
            return time.shift(hours = 9, minutes = 0).isoformat()
         if brevet_dist_km == 400:
            return time.shift(hours = 12, minutes = 8).isoformat()
         if brevet_dist_km == 600:
            return time.shift(hours = 18, minutes = 48).isoformat()
         if brevet_dist_km == 1000:
            return time.shift(hours = 33, minutes = 5).isoformat()
            
      if (control_dist_km >= li[0] and control_dist_km <= li[1]):
         hours, minutes = convert_time((control_dist_km - start_loc)/max_speed)
         time = time.shift(hours = hours, minutes = minutes)
         break
      hours, minutes = convert_time((end_loc - start_loc)/max_speed)
      time = time.shift(hours = hours, minutes = minutes)
   return time.isoformat()

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
   """
   time = arrow.get(brevet_start_time)
   if control_dist_km == 0: #first control, always 1 hr
      return time.shift(hours = 1).isoformat()
   elif control_dist_km <= 60: #french rules
      hours, minutes = convert_time(control_dist_km/ 20 + 1)
      return time.shift(hours=hours, minutes=minutes).isoformat()
   for li in speeds: #every other case
      start_loc, end_loc, min_speed, max_speed = li
      if control_dist_km >= brevet_dist_km:
         #rules for finish points, cleanup later
         if brevet_dist_km == 200:
            return time.shift(hours = 13, minutes = 30).isoformat()
         if brevet_dist_km == 300:
            return time.shift(hours = 20, minutes = 0).isoformat()
         if brevet_dist_km == 400:
            return time.shift(hours = 27, minutes = 0).isoformat()
         if brevet_dist_km == 600:
            return time.shift(hours = 40, minutes = 0).isoformat()
         if brevet_dist_km == 1000:
            return time.shift(hours = 75, minutes = 0).isoformat()
      if control_dist_km >= li[0] and control_dist_km <= li[1]:
         hours, minutes = convert_time((control_dist_km - start_loc)/min_speed)
         time = time.shift(hours = hours, minutes = minutes)
         break
      hours, minutes = convert_time((end_loc - start_loc)/min_speed)
      time = time.shift(hours = hours, minutes = minutes)

   return time.isoformat()


