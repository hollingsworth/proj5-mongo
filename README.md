# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

## Author
Revised by Sydney Hollingsworth, sholling@uoregon.edu
Credits to Michal Young for the initial version of this code.

## Program Function
A calculator for ACP controle times. The algorithm used comes frmo Randonneurs USA, and is described here (https://rusa.org/pages/acp-brevet-control-times-calculator).
Built using AJAX in the frontend, and Flask in the backend, and packaged in a docker container.

Controle times are stored in a MongoDB database
